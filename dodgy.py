from yaml import load
from yaml import CLoader as Loader

from collections import deque
import sys

toplevel = [e for e in sys.argv[1:]]
depkinds = ["depends", "runtime-depends", "build-depends"]

rundeps=["depends", "runtime-depends"]
builddeps=["depends", "build-depends"]


to_load=deque([t for t in toplevel])
loaded = {}
nodes = {}
ncounter = 0

while len(to_load) > 0:
    fname = to_load.popleft()
    if fname not in loaded:
        nodes[fname] = "n{}".format(ncounter)
        ncounter += 1
        with open(fname, "r") as fh:
            content = fh.read()
        data = load(content, Loader=Loader)
        loaded[fname] = data
        for depkind in depkinds:
            for dep in data.get(depkind, []):
                to_load.append(dep)

#print("graph tree {")
#for fname, data in loaded.items():
#    print("  {} [label=\"{}\"];".format(nodes[fname], fname))
#    for depkind in depkinds:
#        for dep in data.get(depkind, []):
#            if depkind in rundeps:
#                print("  {} -- {};".format(nodes[fname], nodes[dep]))
#            if depkind in builddeps:
#                print("  {} -- {} [style=dotted];".format(nodes[fname], nodes[dep]))
#print("}")

print("Loaded {} elements".format(ncounter))
