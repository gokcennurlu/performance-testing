from testbase import *

for fname in sys.stdin:
    fname = fname.strip()
    with open(fname, "r") as fh:
        hashlib.sha256(fh.read().encode('utf-8')).hexdigest()

